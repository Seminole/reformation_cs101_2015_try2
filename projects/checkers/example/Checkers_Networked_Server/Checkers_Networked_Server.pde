import processing.net.*;
//127.0.0.1
Server server;
String[] clientsConnected;
ArrayList <String> messages = new ArrayLists <(String)>() ;
String[] messages;
int serverPort = 5000;

int rand;
int trigger;

void setup() {
  startServer();
  

}

void draw() {
  checkForMessage();
  
  if (millis() > trigger) {
    sendMessage("This is a message from the server at " + millis());
    rand = int(random(10000));
    trigger = millis() + rand;
  }
}

/* This method processes a message from the client. Messages sent between 
   client and server are assumed to be terminated by '\n'. The method assumes
   terminating '\n' has already been removed */
void processMessage(String message) {
print (message + '\n');
 messages.add( millis() + "~" + message);//messages[messages.length] = millis() + "~" + "message";
String[]vals = splitTokens("~");
String client= vals[0];
int msgType = Integer.parseInt(vals[1]); // type of message
switch (msgType) {
  case 0: // move message
  case 1: // text message
}
  
}

/* This method sends a message to the server */
void sendMessage(String message) {
  println("Sending message: " + message);
  server.write(message + '\n');
}

/* This method checks to see if a message has arrived. If so, it removes the 
   terminating '\n' and passes the message on for processing */
void checkForMessage() {
  
  Client client = server.available();
  if (client!=null){
    print("recieving message: ");
    String incomingMessage = client.readStringUntil('\n').trim();
    if(incomingMessage != null && incomingMessage.length()>0){
      processMessage(incomingMessage.trim());   
  }
  else {
    print('\n');
  }}
}

void startServer() {
  server = new Server(this, 5000);
  println("Server listening on port 5000...");
}

/* The code inside serverEvent() is run when a new client connects 
   to a server that has been created within the program. */
void serverEvent(Server server, Client client) {
  println("A new client has connected: " + client.ip());
}