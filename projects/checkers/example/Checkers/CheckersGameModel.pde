// -----------------------------------------------------------------------------
// Blair Watkinson
// Checkers Model
// Developed for CS101 to demonstrate MVC approach to design and development
// -----------------------------------------------------------------------------

class CheckersGameModel {
  
  // ---------------------------------------------------------------------------
  // Declare object properties
  // ---------------------------------------------------------------------------  
  int[][] board = new int[8][8]; 
  int playerTurn = 1;
  int winner = 0;

  // ---------------------------------------------------------------------------
  // Declare object constructor
  // Initialize object properties
  // --------------------------------------------------------------------------- 
  CheckersGameModel() {
    for (int row = 7; row >=0; row--) {
      for (int col = 0; col < 8; col++) {

        // initialize empty board
        board[row][col] = 0;

        // add Player 2 pieces
        if (row >= 5 && (row+col) % 2 == 0) {
          board[row][col] = 2;
        }

        // add Player 1 pieces
        if (row <= 2 && (row+col) % 2 == 0) {
          board[row][col] = 1;
        }
      }
    }
    
    playerTurn = 1;
    winner = 0;
  }

  // ---------------------------------------------------------------------------
  // boolean move(String start, String target) 
  // Primary public method
  // Inputs: a start square and target square as a two-character string of the 
  //   form: a4 or b3
  // Returns: true if the move from start to target is valid, false otherwise
  // Other effects: if the turn is valid, the piece is moved from start to 
  //   target, tbe player turn is updated if appropriate,, and a winner is set
  //   if a player has won the game.
  // ---------------------------------------------------------------------------
  boolean move(String start, String target) {

    // verify there is not already a winner
    if (winner != 0) {
      return false;
    }
    
    // verify the strings are valid
    if (!isValidMoveString(start) || !isValidMoveString(target)) {
      if (DEBUG) println("invalid move strings");
      return false;
    }

    // get startRow, startCol, targetRow, targetCol
    int startRow = getMoveRow(start);
    int startCol = getMoveCol(start);
    int targetRow = getMoveRow(target);
    int targetCol = getMoveCol(target);

    // a valid move is either a move or a jump
    // if a jump is available, it must be taken
    if (!hasJump() && isValidMove(startRow, startCol, targetRow, targetCol)) {
      board[targetRow][targetCol] = board[startRow][startCol];
      board[startRow][startCol] = 0;

      // promote piece, if applicable
      if ((playerTurn == 1 && targetRow == 7) || (playerTurn == 2 && targetRow == 0)) {
        board[targetRow][targetCol] = -1 * abs(board[targetRow][targetCol]);
      }

      playerTurn = playerTurn == 1 ? 2 : 1;
    } else if (isValidJump(startRow, startCol, targetRow, targetCol)) {
      board[targetRow][targetCol] = board[startRow][startCol];
      board[startRow][startCol] = 0;
      board[(startRow + targetRow)/2][(startCol+ targetCol)/2] = 0;

      // promote piece, if applicable
      if ((playerTurn == 1 && targetRow == 7) || (playerTurn == 2 && targetRow == 0)) {
        board[targetRow][targetCol] = -1 * abs(board[targetRow][targetCol]);
      }

      // if the player has no additional jumps it's the other player's turn
      if (!hasJump(targetRow, targetCol)) {
        playerTurn = playerTurn == 1 ? 2 : 1;
      }
    } else {
      return false;
    }

    // if after switching players, the player has no valid moves, the previous player is the winner
    // if that's the case, switch the current player back to the player that most recently moved
    if (!hasMove() && !hasJump()) {
      winner = (playerTurn == 1 ? 2 : 1);
      playerTurn = winner;
    }

    return true;
  }

  // ---------------------------------------------------------------------------
  // int getPlayerTurn() 
  // Accessor method for player turn
  // ---------------------------------------------------------------------------
  int getPlayerTurn() {
    return playerTurn;
  }

  // ---------------------------------------------------------------------------
  // int getWinner()
  // Accessor method for winner
  // ---------------------------------------------------------------------------
  // the winner is the other player if the current player has no valid moves
  int getWinner() {
    return winner;
  }

  // ---------------------------------------------------------------------------
  // boolean isValidMoveString(String move)
  // Input: a string to test as a valid move string
  // Returns: true if the string represents a valid move
  // ---------------------------------------------------------------------------
  boolean isValidMoveString(String move) {
    if (move.charAt(0) >= 'a' && move.charAt(0) <= 'h' && move.charAt(1) >= '0' && move.charAt(1) <= '8') {
      return true;
    } else {
      return false;
    }
  }

  // ---------------------------------------------------------------------------
  // isValidMove(int startRow, int startCol, int targetRow, int targetCol)
  // Input: 4 ints representing a move within the 2d board array
  // Returns: true if the move is a valid move (no jumps) from start to target
  // ---------------------------------------------------------------------------
  boolean isValidMove(int startRow, int startCol, int targetRow, int targetCol) {

    if (max(startRow, startCol) > 7 || max (targetRow, targetCol) > 7 || 
        min(startRow, startCol) < 0 || min(targetRow, targetCol) < 0) {
      return false;
    }

    // ensure the start location is occupied by the current player
    if (abs(board[startRow][startCol]) != playerTurn) {
      return false;
    }

    // ensure the target location is empty
    if (board[targetRow][targetCol] != 0) {
      return false;
    }

    // if the piece is a king, direction doesn't matter
    if (board[startRow][startCol] < 0) {
      if (abs(targetRow - startRow) == 1 &&  abs(targetCol - startCol) == 1) {
        return true;
      }
    } else {
      int direction = playerTurn == 1 ? 1 : -1;
      if (startRow + direction == targetRow && abs(targetCol - startCol) == 1) {
        return true;
      }
    }
    return false;
  }

  // ---------------------------------------------------------------------------
  // isValidJump(int startRow, int startCol, int targetRow, int targetCol)
  // Input: 4 ints representing a move within the 2d board array
  // Returns: true if the move is a valid jump from start to target
  // ---------------------------------------------------------------------------
  boolean isValidJump(int startRow, int startCol, int targetRow, int targetCol) {

    if (max(startRow, startCol) > 7 || max (targetRow, targetCol) > 7 || min(startRow, startCol) < 0 || min(targetRow, targetCol) < 0) {
      return false;
    }

    // ensure the start location is occupied by the current player
    if (abs(board[startRow][startCol]) != playerTurn) {
      return false;
    }

    // ensure the target location is empty
    if (board[targetRow][targetCol] != 0) {
      if (DEBUG) println("Target position is not empty");
      return false;
    }

    // if the piece being jumped doesn't below to the opposing player, it's an invalid jump
    if (abs(board[(startRow + targetRow)/2][(startCol + targetCol)/2]) != (playerTurn == 1 ? 2 : 1)) {
      return false;
    }

    // if the piece is a king, direction doesn't matter
    if (board[startRow][startCol] < 0) {
      if (abs(targetRow - startRow) == 2 &&  abs(targetCol - startCol) == 2) {
        return true;
      }
    } else {
      int direction = playerTurn == 1 ? 2 : -2;
      if (startRow + direction == targetRow && abs(targetCol - startCol) == 2) {
        return true;
      }
    }
    return false;
  }

  // ---------------------------------------------------------------------------
  // boolean hasMove(int startRow, int startCol)
  // Input: 2 ints representing a starting position for a piece
  // Returns: true if the piece at the starting position has a valid move (not
  //   jumps).  false otherwise
  // ---------------------------------------------------------------------------
  boolean hasMove(int startRow, int startCol) {

    for (int row = -1; row <=1; row +=2) {
      for (int col = -1; col <=1; col +=2) {
        if (isValidMove(startRow, startCol, startRow + row, startCol + col)) {
          return true;
        }
      }
    }
    return false;    
  }

  // ---------------------------------------------------------------------------
  // boolean hasJump(int startRow, int startCol)
  // Input: 2 ints representing a starting position for a piece
  // Returns: true if the piece at the starting position has a valid jump (not
  //   jumps).  false otherwise
  // ---------------------------------------------------------------------------
  boolean hasJump(int startRow, int startCol) {
    for (int row = -2; row <=2; row +=4) {
      for (int col = -2; col <=2; col +=4) {
        if (isValidJump(startRow, startCol, startRow + row, startCol + col)) {
          return true;
        }
      }
    }

    return false;
  }

  // ---------------------------------------------------------------------------
  // boolean hasMove()
  // Returns: true if the current player has any valid move (no jumps)
  // ---------------------------------------------------------------------------
  boolean hasMove() {
    for (int row = 0; row < 8; row++) {
      for (int col = 0; col < 8; col++) {
        if (abs(board[row][col]) == playerTurn && hasMove(row, col)) {
          return true;
        }
      }
    }
    return false;
  }

  // ---------------------------------------------------------------------------
  // boolean hasJump()
  // Returns: true if the current player has any valid move (no jumps)
  // ---------------------------------------------------------------------------
  boolean hasJump() {
    for (int row = 0; row < 8; row++) {
      for (int col = 0; col < 8; col++) {
        if (abs(board[row][col]) == playerTurn && hasJump(row, col)) {
          return true;
        }
      }
    }
    return false;
  }

  // ---------------------------------------------------------------------------
  // int getMoveRow(String move)
  // Inputs: a string representing a move
  // Returns: the row of the move string
  // ---------------------------------------------------------------------------
  int getMoveRow(String move) {
    return int(move.charAt(1) - 48) - 1;
  }

  // ---------------------------------------------------------------------------
  // int getMoveCol(String move)
  // Inputs: a string representing a move
  // Returns: the column of the move string
  // ---------------------------------------------------------------------------
  int getMoveCol(String move) {
    return int(move.charAt(0) - 97);
  }

  // ---------------------------------------------------------------------------
  // String toString()
  // Returns: s string representing the board state
  // ---------------------------------------------------------------------------
  String toString() {
    String resultString = "";

    for (int row = 7; row >= 0; row--) {
      resultString += row + 1 + "|";
      for (int col = 0; col < 8; col++) {
        switch(board[row][col]) {
        case 0: 
          resultString += " "; 
          break;
        case 1: 
          resultString += "o"; 
          break;
        case 2: 
          resultString += "x"; 
          break;
        case -1: 
          resultString += "O"; 
          break;
        case -2: 
          resultString += "X"; 
          break;
        default: 
          resultString += "~";
        }
        resultString += "|";
      }
      resultString += row + "\n";
    }
    resultString += "  a b c d e f g h\n\n";

    return resultString;
  }
}