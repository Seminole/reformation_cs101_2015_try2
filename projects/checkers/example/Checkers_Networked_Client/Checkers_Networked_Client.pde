import processing.net.*;

Client client;
String serverIP = "127.0.0.1";
int serverPort = 5000;
String uniqueID;

int rand;
int trigger;

void setup() {
  connectToServer();
}

void draw() {
  if (millis() > trigger) {
    sendMessage("This is a message from the client at " + millis());
    rand = int(random(10000));
    trigger = millis() + rand;
  }
}

/* This method processes a message from the server. Messages sent between 
 client and server are assumed to be terminated by '\n'. The method assumes
 terminating '\n' has already been removed */
void processMessage(String message) {
}

/* This method sends a message to the server. All messages are prepended with
 the client's unique ID */
void sendMessage(String message) {
  println("Sending message: " + message);
  client.write(uniqueID + "," + message + '\n');
}

/* This method connects this client to the server */
void connectToServer() {
  client = new Client(this, serverIP, serverPort);
  uniqueID = Integer.toHexString(client.hashCode()) + int(random(1000000));
  registerWithServer();
  println("Connected to server at " + serverIP + ":" + serverPort);
}

/* This metod send a single string to the server notifying the server of its
 unique ID handler */
void registerWithServer() {
  println("Registering as " + uniqueID + " with server");
  client.write(uniqueID + '\n');
}

/* This function is called when a server sends a byte to
 an existing Client object. */
void clientEvent(Client client) {
  processMessage(client.readStringUntil('\n'));
}